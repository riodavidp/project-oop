print("============SELAMAT DATANG DI ATM RIO's============")
print("===Silahkan Masukkan Kartu ATM Anda Di Mesin ATM===")
class Atm:
    def __init__(self):
        self.no_rekening = 73287263 
        self.pin = 123456
        self.saldo = 1000000
        input_no_rekening = int(input("Masukkan No Rekening Anda        : "))
        input_pin = int(input("Masukkan Pin Anda                : "))
        if input_no_rekening == self.no_rekening:
            if input_pin == self.pin:
                print("No Rek dan Pin Yang Anda Masukkan Benar " )
            else:
                pin = int(input("Masukkan Pin Dengan Benar     :"))
        else:
            no_rekening = int(input("Masukkan No Rekening Yang Benar         :"))
        # Pilihan menu
        print("""Pilih menu yang ingin dilakukan
              1. Cek Saldo
              2. Tarik Tunai
              3. Transfer
              4. Setor Tunai
              5. Exit""")
        option = int(input("Masukkan pilihan menu anda       : "))
        if option == 1:
            self.cek_saldo()
        elif option == 2:
            self.tarik_tunai()
        elif option == 3:
            self.transfer()
        elif option == 4:
            self.setor_tunai()
        elif option == 5:
            self.exit()
    def cek_saldo(self):
        input_pin = int(input("Masukkan Pin Anda                : "))
        if input_pin == self.pin:
            print(f"Saldo Anda Adalah {self.saldo}")
        else:
            print("Pin Yang Anda Masukkan Salah")
    def tarik_tunai(self):
        input_pin = int(input("Masukkan Pin Anda                : "))
        if input_pin == self.pin:
            input_tarik_tunai = int(input("Masukkan Jumlah Penarikan Anda   : "))
            if input_tarik_tunai <= self.saldo:
                self.saldo = self.saldo - input_tarik_tunai
                print(f"Saldo Pembaruan Anda Adalah {self.saldo}")
            else:
                print("Anda Tidak Memiliki Saldo Yang Cukup")
        else:
            print("Pin Yang Anda Masukkan Salah")
    def transfer(self):
        input_pin = int(input("Masukkan Pin Anda                : "))
        if input_pin == self.pin:
            input_no_rekening = int(input("Masukkan Nomer Rekening Yang Dituju : "))
            input_transfer = int(input("Masukkan Jumlah Transfer         : "))
            if input_transfer <= self.saldo:
                self.saldo = self.saldo - input_transfer
                print(f"Saldo Pembaruan Anda Adalah {self.saldo}")
            else:
                print("Anda Tidak Memiliki Saldo Yang Cukup")
        else:
            print("Pin Yang Anda Masukkan Salah")
    def setor_tunai(self):
        input_pin = int(input("Masukkan Pin Anda                : "))
        if input_pin == self.pin:
            input_setor_tunai = int(input("Masukkan Jumlah Setor Tunai Anda : "))
            self.saldo = self.saldo + input_setor_tunai
            print(f"Saldo Pembaruan Anda Adalah {self.saldo} ")
        else:
            print("Pin Yang Anda Masukkan Salah")
    def exit(self):
        print("======Silahkan Keluarkan Kartu ATM Dari Mesin======")
        print("====================Terimakasih====================")

atm1 = Atm()